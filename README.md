# Hello, Rust!

This is my first stab at making something in rust, to learn the language.

This is a rather useless text editor -- enjoy it!

## First Try:

I tried to make a one-line text editor, before formally reading any rust docs

Just try and muddle through :)

## Second and Third Try:

For the second and third try I revisited this project after reading 
***The Rust Programming Language (2nd Ed)***. My goal was to use as little
online documentation/search engining as possible -- just use the book,
the compiler, and my wits.

### The Second try:

A somewhat abortive attempt at making a more fully-featured text editor using 
the String type. However I did not understand quite how IO worked in Rust
and found myself struggling to read in strings without line buffering.

(I gave up. This one is totally busted.)

### The Third Try:

I learned to love myself, and used the [Console](https://docs.rs/console/0.15.8/console/index.html) library to handle:
- reading a single key from the terminal (rather than trying to hack my own)
- positioning the cursor and writing to the terminal

This allowed me to handle arrow keys, backspace, delete, and escape

## Running it
If you'd like to try it yourself:

```sh
cd third-try
cargo run
```

### Keys:
- Up, Down, Left, Right 
    - move the cursor
- Backspace
    - delete the character before the cursor, move the cursor left 1 space.
    - If the cursor is at the beginning of the line, concat this line with the previous
- Delete
    - delete the character on the cursor, and do not move
    - does nothing at EOL
- Escape
    - save buffer to "output.txt", and exit the program
- Newline
    - Insert a new line after the current line.
- All others:
    - Attempt to insert a character at the current position.
