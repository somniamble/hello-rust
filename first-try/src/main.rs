use std::io::{self, Write};

const ADD: &str = "ADD";
const RM: &str = "rm";
const LEFT: &str = "<";
const RIGHT: &str = ">";

struct State {
    items: Vec<String>,
    position: usize,
}

impl State {
    fn new() -> State {
        State {
            items: Vec::new(),
            position: 0,
        }
    }

    fn empty(&self) -> bool {
        return self.items.len() == 0;
    }

    fn left(&mut self) -> &State {
        // no change
        if self.empty() {
            return self;
        } else if self.position == 0 {
            return self;
        } else {
            self.position = self.position - 1;
            return self;
        }
    }

    fn right(&mut self) -> &State {
        // no change
        if self.empty() {
            return self;
        } else if self.position == self.items.len() {
            return self;
        } else {
            self.position = self.position + 1;
            return self;
        }
    }

    fn add(&mut self, item: String) -> &State {
        if self.empty() {
            self.items.push(item);
            self.position = 1;
        } else {
            // insert after point
            self.items.insert(self.position, item);
        }
        return self;
    }

    fn rm(&mut self) -> &State {
        if self.items.len() == 1 {
            self.items.pop();
            self.position = 0;
        } else {
            let to_remove = if self.position == 0 {
                0
            } else {
                self.position - 1
            };
            self.items.remove(to_remove);
            self.left();
        }
        return self;
    }

    fn command(&mut self, cmd: String) -> &State {
        match cmd.as_str() {
            // ADD => {
            //     println!("What do you want to add? >");
            //     let item = readl();
            //     return self.add(item);
            // }
            RM => return self.rm(),
            LEFT => return self.left(),
            RIGHT => return self.right(),
            "" => return self,
            _ => return self.add(cmd),
        }
    }

    fn current(&self) -> String {
        if self.empty() {
            return "".to_string();
        } else if self.position == 0 {
            return self.items[0].clone();
        } else {
            return self.items[self.position - 1].clone();
        }
    }
}

fn positions_of_strings(strs: Vec<String>) -> Vec<usize> {
    let mut positions = Vec::new();
    let mut current: usize = 0;
    positions.push(current);
    for s in strs {
        current += s.len() + 1;
        positions.push(current.clone());
    }
    return positions;
}

fn format_state(state: &State) -> String {
    if state.empty() {
        return String::from("State is empty. Add something to get started.");
    } else {
        let row1 = state.items.join(" ");
        let positions = positions_of_strings(state.items.clone());
        let position = positions[state.position];
        let mut position_row = std::iter::repeat(" ").take(position).collect::<String>();
        position_row.replace_range(position..position, "^");
        let fmted = format!(
            "{}\n{}",
            row1,
            position_row
        );
        return fmted;
    }
}

fn readl() -> String {
    let mut buffer = String::new();
    match io::stdin().read_line(&mut buffer) {
        Ok(_) => return buffer.as_str().trim().to_string(),
        Err(_) => return String::from("ERR"),
    }
}

fn main() {
    let mut state = State::new();
    loop {
        let output = format_state(&state);
        // clear terminal and move to 1:1
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
        println!("{}", output);
        print!("$ ");
        io::stdout().flush();
        let cmd = readl();
        state.command(cmd);
    }
}
