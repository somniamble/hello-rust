# Useless Text Editor

## Running:
`cargo run`

## Commands:

- `rm` - delete entry preceding cursor
    - if the cursor is at 0, delete first entry.
- `<` - move cursor left
- `>` - move cursor right
