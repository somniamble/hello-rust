use std::io::{self, Read};


#[derive(Debug)]
enum Command {
    Delete,
    Backspace,
    Add(String),
    Left,
    Right,
    Up,
    Down
    // Unknown
}

impl Command {
    fn from_string(s: String) -> Command {
        match s.as_str() {
            "\x7F" => Command::Delete,
            "\x08" => Command::Backspace,
            "^[[C" => Command::Left,
            "^[[D" => Command::Right,
            "^[[A" => Command::Up,
            "^[[B" => Command::Down,
            string => Command::Add(string.to_owned()),
            //c => Command::Unknown(c)
        }
    }
}

struct State {
    buffer: String,
    position: usize,
    messages: Vec<String>,
}

fn is_printable(c: char) -> bool {
    return !char::is_control(c);
}

impl Clone for State {
    fn clone(&self) -> State {
        State {
            buffer: self.buffer.clone(),
            position: self.position,
            messages: self.messages.clone()
        }
    }
}

impl State {
    fn new() -> State {
        State {
            buffer: "".to_owned(),
            position: 0,
            messages: Vec::new()
        }
    }

    fn is_empty(&self) -> bool {
        return self.buffer.len() == 0;
    }

    fn cursor_at_eol(&self) -> bool {
        return self.position == self.buffer.len();
    }

    fn left(&self) -> State {
        if self.is_empty() || self.position == 0 {
            return self.clone();
        } else {
            State {
                buffer: self.buffer.clone(),
                position: self.position - 1,
                messages: vec![format!("moved to {}", self.position - 1)]
            }
        }
    }

    fn right(&self) -> State {
        if self.is_empty() || self.position == self.buffer.len() {
            return self.clone();
        } else {
            State {
                buffer: self.buffer.clone(),
                position: self.position + 1,
                messages: vec![format!("moved to {}", self.position + 1)]
            }
        }
    }

    fn add(&self, item: String) -> State {
        if self.is_empty() {
            return State {
                buffer: item,
                position: 1,
                messages: vec![]
            }
        } else if self.cursor_at_eol() {
            let mut buffer = self.buffer.clone();
            buffer.push_str(item.as_str());
            return State {
                buffer,
                position: self.position + 1,
                messages: vec![format!("Added {} to end of line", item)]
            }
        }
        else {
            let chars: Vec<char> = self.buffer.clone().chars().collect();
            let left = &chars[0..self.position];
            let right = &chars[self.position..];
            let left_str: String = left.into_iter().collect();
            let right_str: String = right.into_iter().collect();
            // I'm sure there's a more efficient way to do this
            let buffer = format!("{}{}{}",
                    left_str,
                    item,
                    right_str
                );
            return State {
                buffer,
                position: self.position + item.len(),
                messages: vec![format!("Added item of length {} to position {}", item.len(), self.position)]
            }
        }
    }

    // Delete at point
    fn del(&self) -> State {
        if self.buffer.len() <= 1 {
            return State::new();
        }
        // Do nothing when calling delete at end of line
        else if self.cursor_at_eol() {
            return self.clone();
        }
        else {
            let chars: Vec<char> = self.buffer.clone().chars().collect();
            // exclude current position
            let left = &chars[..self.position];
            let right = &chars[self.position + 1..];
            let buffer = left.iter().chain(right.iter()).collect();
            State {
                buffer,
                position: self.position - 1,
                messages: vec![format!("Removed char at position {}", self.position)]
            }
        }
    }

    // Delete before point
    fn backspace(&self) -> State {
        if self.buffer.len() <= 1 {
            return State::new();
        }
        else if self.position == 0 {
            return self.clone();
        }
        else {
            let chars: Vec<char> = self.buffer.clone().chars().collect();
            // delete before position
            let left = &chars[..self.position - 1];
            let right = &chars[self.position..];
            let buffer = left.iter().chain(right.iter()).collect();
            State {
                buffer,
                position: self.position - 1,
                messages: vec![format!("Removed char at position {}", self.position)]
            }
        }
    }

    fn handle(&self, cmd: Command) -> State {
        match cmd {
            Command::Delete => self.del(),
            Command::Backspace=> self.backspace(),
            Command::Left => self.left(),
            Command::Right => self.right(),
            Command::Add(item) => self.add(item),
            cmd => 
                State {
                    buffer: self.buffer.clone(),
                    position: self.position,
                    messages: vec![format!("Unsure of how to handle command: {:?}", cmd)]
                }
            // Command::Unknown(control_code) => {
            //     // very silly
            //     println!("Unsure of how to handle char with bytes: {:?}", control_code.to_string().as_bytes());
            //     return self.to_owned();
            // }
        }
    }

    fn render(&self) -> String {
        // why does this work?
        return self.buffer.to_owned();
        //return self.buffer.clone().into_iter().collect();
    }
}

fn main() -> io::Result<()> {
    let mut state = State::new();
    let mut buf = String::new();
    loop {
        let output = state.render();
        // clear terminal and move to 1:1
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
        println!("{:?}", state.messages);
        println!("{}", output);

        io::stdin().read_to_string(&mut buf).expect("Should have read");
        let cmd = Command::from_string(buf.clone());
        state = state.handle(cmd);
    }
}
