use console::{Key, Term};
use std::io::{self, Read};
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
enum Command {
    Delete,
    Backspace,
    Add(char),
    Left,
    Right,
    Up,
    Down,
    Newline,
    Unknown,
}

impl Command {
    fn from_key(key: console::Key) -> Command {
        match key {
            Key::Del => Command::Delete,
            Key::Backspace => Command::Backspace,
            Key::ArrowLeft => Command::Left,
            Key::ArrowRight => Command::Right,
            Key::ArrowUp => Command::Up,
            Key::ArrowDown => Command::Down,
            Key::Enter => Command::Newline,
            Key::Char(c) => Command::Add(c),
            _ => Command::Unknown,
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Position {
    line: usize,
    column: usize,
}

impl ToString for Position {
    fn to_string(&self) -> String {
        return format!("{}:{}", self.line, self.column);
    }
}

impl Position {
    fn new() -> Position {
        Position { column: 0, line: 0 }
    }

    fn left(&self) -> Position {
        Position {
            column: if self.column > 0 { self.column - 1 } else { 0 },
            line: self.line,
        }
    }

    fn right(&self, max: usize) -> Position {
        Position {
            column: if self.column < max {
                self.column + 1
            } else {
                max
            },
            line: self.line,
        }
    }

    fn up(&self, max_column: usize) -> Position {
        Position {
            column: if self.column < max_column {
                self.column
            } else {
                max_column
            },
            line: if self.line > 0 { self.line - 1 } else { 0 },
        }
    }

    fn down(&self, max_line: usize, max_column: usize) -> Position {
        Position {
            column: if self.column < max_column {
                self.column
            } else {
                max_column
            },
            line: if self.line < max_line - 1 {
                self.line + 1
            } else {
                max_line - 1
            },
        }
    }
}

#[derive(Debug, Clone)]
struct State {
    buffer: Vec<Vec<char>>,
    position: Position,
}

fn is_printable(c: char) -> bool {
    return !c.is_control() && c.is_ascii();
}

// impl Clone for State {
//     fn clone(&self) -> State {
//         State {
//             buffer: self.buffer.clone(),
//             position: self.position,
//         }
//     }
// }

impl State {
    fn new() -> State {
        State {
            // A buffer with 1 empty line
            buffer: vec![Vec::new()],
            position: Position::new(),
        }
    }

    fn is_empty(&self) -> bool {
        return self.buffer.len() == 1 && self.buffer[0].len() == 0;
    }

    fn cursor_at_eol(&self) -> bool {
        return self.position.column == self.current_line_length();
    }

    fn prev_line_length(&self) -> usize {
        return if self.position.line > 0 {
            self.buffer[self.position.line - 1].len()
        } else {
            0
        };
    }

    fn next_line_length(&self) -> usize {
        return if self.position.line < self.buffer.len() - 1 {
            self.buffer[self.position.line + 1].len()
        } else {
            0
        };
    }

    fn current_line_length(&self) -> usize {
        return self.buffer[self.position.line].len();
    }

    fn left(&self) -> State {
        State {
            buffer: self.buffer.clone(),
            position: self.position.left(),
        }
    }

    fn right(&self) -> State {
        State {
            buffer: self.buffer.clone(),
            position: self.position.right(self.current_line_length()),
        }
    }

    fn up(&self) -> State {
        State {
            buffer: self.buffer.clone(),
            position: self.position.up(self.prev_line_length()),
        }
    }

    fn down(&self) -> State {
        State {
            buffer: self.buffer.clone(),
            position: self
                .position
                .down(self.buffer.len(), self.next_line_length()),
        }
    }

    fn add(&self, item: char) -> State {
        if self.is_empty() {
            return State {
                buffer: vec![vec![item]],
                position: Position { line: 0, column: 1 },
            };
        } else if self.cursor_at_eol() {
            let mut buffer = self.buffer.clone();
            buffer[self.position.line].push(item);
            return State {
                buffer,
                position: Position {
                    column: self.position.column + 1,
                    line: self.position.line,
                },
            };
        } else {
            let mut buffer = self.buffer.clone();
            buffer[self.position.line].insert(self.position.column, item);
            return State {
                buffer,
                position: Position {
                    column: self.position.column + 1,
                    line: self.position.line,
                },
            };
        }
    }

    // Delete at point. Position stays the same.
    fn del(&self) -> State {
        if self.buffer.is_empty() {
            return State::new();
        }
        // Do nothing when calling delete at end of line
        else if self.cursor_at_eol() {
            return self.clone();
        } else {
            let mut buffer = self.buffer.clone();
            buffer[self.position.line].remove(self.position.column);
            State {
                buffer,
                position: self.position,
            }
        }
    }

    // Delete before point. Position moves back 1 column.
    fn backspace(&self) -> State {
        if self.is_empty() {
            return State::new();
        } else if self.position.column == 0 {
            if self.position.line == 0 {
                return self.clone();
            } else {
                // concatenate current line with prev
                let mut buffer = self.buffer.clone();
                let mut current_line = buffer.remove(self.position.line);
                buffer[self.position.line - 1].append(&mut current_line);
                return State {
                    buffer,
                    position: Position {
                        column: self.prev_line_length(),
                        line: self.position.line - 1,
                    },
                };
            }
        } else {
            let mut buffer = self.buffer.clone();
            buffer[self.position.line].remove(self.position.column - 1);
            State {
                buffer,
                position: Position {
                    column: self.position.column - 1,
                    line: self.position.line,
                },
            }
        }
    }

    fn newline(&self) -> State {
        let mut buffer = self.buffer.clone();
        buffer.insert(self.position.line + 1, Vec::new());
        return State {
            buffer,
            position: Position {
                line: self.position.line + 1,
                column: 0,
            },
        };
    }

    fn handle(&self, cmd: Command) -> State {
        match cmd {
            Command::Delete => self.del(),
            Command::Backspace => self.backspace(),
            Command::Left => self.left(),
            Command::Right => self.right(),
            Command::Up => self.up(),
            Command::Down => self.down(),
            Command::Newline => self.newline(),
            Command::Add(item) => self.add(item),
            Command::Unknown => {
                // very silly
                println!("Unsure of how to handle unknown command",);
                return self.to_owned();
            }
        }
    }

    fn render(&self) -> String {
        return self
            .buffer
            .clone()
            .into_iter()
            .map(String::from_iter)
            .collect::<Vec<_>>()
            .join("\n");
    }
}

fn main() -> io::Result<()> {
    let mut state = State::new();
    let term = Term::stdout();
    loop {
        let output = state.render();
        let (last_line, _) = term.size();
        // clear terminal and move to 1:1
        term.clear_screen().expect("should have cleared");

        // write position
        term.move_cursor_to(0, last_line.into())?;
        term.write_line(state.position.to_string().as_str())?;

        // write buffer
        term.move_cursor_to(0, 0)?;
        term.write_line(output.as_str())
            .expect("Should have written");

        // draw cursor at current position
        term.move_cursor_to(state.position.column, state.position.line)
            .expect("should have moved cursor");

        let k = term.read_key().unwrap_or(Key::Unknown);

        // hit Escape to save buffer to output.txt and quit
        if (k == Key::Escape) {
            let mut file = File::create("output.txt")?;
            file.write_all(output.as_str().as_bytes());
            println!("\nWriting to `output.txt`");
            return Ok(());
        }
        // println!("{:?}", k);
        let cmd = Command::from_key(k);
        state = state.handle(cmd);
    }
    // io::stdin()
    //     .read_to_string(&mut buf)
    //     .expect("Should have read");
    // for c in buf.chars() {
    //     let cmd = Command::from_char(c);
    //     state = state.handle(cmd);
    // }
}
